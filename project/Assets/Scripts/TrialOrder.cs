﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

//Randomize trial order and control the movement to each trial/room

public class TrialOrder : MonoBehaviour {

    public GameObject Player;
    public Vector3 r1start, r2start, r3start, r4start, r5start; //starting position for each room

    private Vector3 t1start, t2start, t3start, t4start, t5start; //to assign the starting position of each room to their respective trial
    private Vector3 t6start, t7start, t8start, t9start, t10start;
    private List<GameObject> trials1 = new List<GameObject>(); //create list for rooms
    private List<GameObject> trials2 = new List<GameObject>(); //list for second block
    private List<Vector3> b1startlist = new List<Vector3>();
    private List<Vector3> b2startlist = new List<Vector3>();

    //string Ltype;
    string block1;
    string block2;
    int trialCount = 0;

    
    void Start () {

        //add the rooms to the list
        foreach (GameObject room in GameObject.FindGameObjectsWithTag("testrooms"))
        {
            trials1.Add(room);
            trials2.Add(room);
            Debug.Log(room.name);
        }

        //check that 5 have been added
        Debug.Log(trials1.Count);

        //shuffle the order of the rooms and print that order to a text file
       Shuffle(trials1);
       Shuffle(trials2);
       WriteString(trials1, trials2);

        //if room, give start pos
        switch (trials1[0].name)
        {
            case "room1":
                t1start = r1start;
                break;
            case "room2":
                t1start = r2start;
                break;
            case "room3":
                t1start = r3start;
                break;
            case "room4":
                t1start = r4start;
                break;
            case "room5":
                t1start = r5start;
                break;
        }

        switch (trials1[1].name)
        {
            case "room1":
                t2start = r1start;
                break;
            case "room2":
                t2start = r2start;
                break;
            case "room3":
                t2start = r3start;
                break;
            case "room4":
                t2start = r4start;
                break;
            case "room5":
                t2start = r5start;
                break;
        }

        switch (trials1[2].name)
        {
            case "room1":
                t3start = r1start;
                break;
            case "room2":
                t3start = r2start;
                break;
            case "room3":
                t3start = r3start;
                break;
            case "room4":
                t3start = r4start;
                break;
            case "room5":
                t3start = r5start;
                break;
        }

        switch (trials1[3].name)
        {
            case "room1":
                t4start = r1start;
                break;
            case "room2":
                t4start = r2start;
                break;
            case "room3":
                t4start = r3start;
                break;
            case "room4":
                t4start = r4start;
                break;
            case "room5":
                t4start = r5start;
                break;
        }

        switch (trials1[4].name)
        {
            case "room1":
                t5start = r1start;
                break;
            case "room2":
                t5start = r2start;
                break;
            case "room3":
                t5start = r3start;
                break;
            case "room4":
                t5start = r4start;
                break;
            case "room5":
                t5start = r5start;
                break;
        }

        Vector3[] input = {t1start, t2start, t3start, t4start, t5start};
        b1startlist.AddRange(input);

        //block2
        switch (trials2[0].name)
        {
            case "room1":
                t6start = r1start;
                break;
            case "room2":
                t6start = r2start;
                break;
            case "room3":
                t6start = r3start;
                break;
            case "room4":
                t6start = r4start;
                break;
            case "room5":
                t6start = r5start;
                break;
        }

        switch (trials2[1].name)
        {
            case "room1":
                t7start = r1start;
                break;
            case "room2":
                t7start = r2start;
                break;
            case "room3":
                t7start = r3start;
                break;
            case "room4":
                t7start = r4start;
                break;
            case "room5":
                t7start = r5start;
                break;
        }

        switch (trials2[2].name)
        {
            case "room1":
                t8start = r1start;
                break;
            case "room2":
                t8start = r2start;
                break;
            case "room3":
                t8start = r3start;
                break;
            case "room4":
                t8start = r4start;
                break;
            case "room5":
                t8start = r5start;
                break;
        }

        switch (trials2[3].name)
        {
            case "room1":
                t9start = r1start;
                break;
            case "room2":
                t9start = r2start;
                break;
            case "room3":
                t9start = r3start;
                break;
            case "room4":
                t9start = r4start;
                break;
            case "room5":
                t9start = r5start;
                break;
        }

        switch (trials2[4].name)
        {
            case "room1":
                t10start = r1start;
                break;
            case "room2":
                t10start = r2start;
                break;
            case "room3":
                t10start = r3start;
                break;
            case "room4":
                t10start = r4start;
                break;
            case "room5":
                t10start = r5start;
                break;
        }

        Vector3[] input2 = {t6start, t7start, t8start, t9start, t10start};
        b2startlist.AddRange(input2);
    }
	

	void Update () {

        if (Input.GetKeyDown(KeyCode.Q) && trialCount <= 4)
        {
            while (block1 == "Joystick")
            {
                Player.GetComponentInChildren<JoystickController>().enabled = true;
                Player.GetComponentInChildren<TeleporterController>().enabled = false;
            }
            while (block1 == "Teleport")
            {
                Player.GetComponentInChildren<JoystickController>().enabled = false;
                Player.GetComponentInChildren<TeleporterController>().enabled = true;
            }

            Player.transform.position = trials1[trialCount].transform.position + trials1[trialCount].transform.TransformDirection(b1startlist[trialCount]);
            trialCount++;
        }

        if (OVRInput.GetDown(OVRInput.Button.Four) && trialCount >= 5 && trialCount < 10) //Input.GetKeyDown("space")
        {
            if (block2 == "Joystick")
            {
                Player.GetComponentInChildren<JoystickController>().enabled = true;
                Player.GetComponentInChildren<TeleporterController>().enabled = false;
            }
            if (block2 == "Teleport")
            {
                Player.GetComponentInChildren<JoystickController>().enabled = false;
                Player.GetComponentInChildren<TeleporterController>().enabled = true;
            }

            Player.transform.position = trials2[trialCount-5].transform.position + trials2[trialCount-5].transform.TransformDirection(b2startlist[trialCount-5]);
            trialCount++;
        }
    }


    void Shuffle(IList<GameObject> list) //Shuffles list of rooms
    {
        int listLength = list.Count;
        Debug.Log("n=" + listLength);

        for (int t = 0; t < listLength; t++)
        {
            GameObject tmp = list[t];
            int r = Random.Range(t, listLength);
            list[t] = list[r];
            list[r] = tmp;
        }

        foreach (GameObject room in list) //prints new order
        {
            Debug.Log(room.name);
        }
    }

    void WriteString(IList<GameObject> list1, IList<GameObject> list2) //write file
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Participant Number", "InitialName.txt", "txt", "Please select file name to save pnum to:");
        if (!string.IsNullOrEmpty(path))
        {
        }

        StreamWriter writer = new StreamWriter(path, true);

        if (path.Contains("JT"))
        {
            block1 = "Joystick";
            block2 = "Teleport";
        }

        if (path.Contains("TJ"))
        {
            block1 = "Teleport";
            block2 = "Joystick";
        }

        writer.WriteLine(System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));

        writer.WriteLine("Block 1: " + block1);
        foreach (GameObject room in list1)
        {
            writer.WriteLine(room.name);
        }

        writer.WriteLine("Block 2: " + block2);
        foreach (GameObject room in list2)
        {
            writer.WriteLine(room.name);
        }

        writer.Close();

        //Re-import the file to update the reference in the editor
        //AssetDatabase.ImportAsset(path);
        //TextAsset asset = Resources.Load("test");

        ////Print the text from the file
        //Debug.Log(asset.text);
    }


}
