﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPointsControl : MonoBehaviour {

    public GameObject point1;
    public GameObject point2;
    public GameObject point3;

    //private bool p1col = false;
    //private bool p2col = false;

	// Use this for initialization
	void Start () {
        MeshRenderer p2render = point2.GetComponent<MeshRenderer>();
        p2render.enabled = false;

        MeshRenderer p3render = point3.GetComponent<MeshRenderer>();
        p3render.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            MeshRenderer p2render = point2.GetComponent<MeshRenderer>();
            p2render.enabled = true;

            MeshRenderer p1render = point1.GetComponent<MeshRenderer>();
            p1render.enabled = false;

            MeshRenderer p3render = point3.GetComponent<MeshRenderer>();
            p3render.enabled = false;
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            MeshRenderer p3render = point3.GetComponent<MeshRenderer>();
            p3render.enabled = true;

            MeshRenderer p1render = point1.GetComponent<MeshRenderer>();
            p1render.enabled = false;

            MeshRenderer p2render = point2.GetComponent<MeshRenderer>();
            p2render.enabled = false;
        }
    }

    //void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject == point1)
    //    {
    //        p1col = true;
    //    }

    //    if (col.gameObject == point2)
    //    {
    //        p2col = true;
    //    }
    //}
}
