﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class JoystickController : MonoBehaviour
{

   // public static Joystick instance;

    public GameObject PlayerCamera, PlayerObject;
    public float speedMod = .05f;
    private bool willMove = false;

    private void Start()
    {
       // instance = this;

    }


    void FixedUpdate()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            willMove = true;
        }
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger)|| Input.GetKeyUp(KeyCode.UpArrow))
        {
            willMove = false;
        }
        if (willMove)
        {
            PlayerObject.transform.position = PlayerObject.transform.position + PlayerCamera.transform.forward * speedMod * Time.deltaTime;
            PlayerObject.transform.position = new Vector3(PlayerObject.transform.position.x, 0, PlayerObject.transform.position.z);
        }

    }

    public void WMFalse()
    {
        willMove = false;
    }
}